import 'package:flutter/material.dart';
import 'package:retos_discord_flutter/src/pages/home_page.dart';
import 'package:retos_discord_flutter/src/pages/reto3/home_page.dart';
import 'package:retos_discord_flutter/src/pages/reto3/loading_page.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: HomePage(),
      routes: {
        'home3': (BuildContext context) => HomeReto3(),
        'loading3': (BuildContext context) => LoadingReto3(),
      },
    );
  }
}