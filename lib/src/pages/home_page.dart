import 'package:flutter/material.dart';
import 'package:retos_discord_flutter/src/pages/reto1/home_page.dart';
import 'package:retos_discord_flutter/src/pages/reto2/home_page.dart';
import 'package:retos_discord_flutter/src/pages/reto3/home_page.dart';
import 'package:retos_discord_flutter/src/pages/reto3/loading_page.dart';
import 'package:retos_discord_flutter/src/pages/reto4/home_page.dart';
import 'package:retos_discord_flutter/src/utils/transition_page.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Retos discord locos por Flutter'),
      ),
      body: SafeArea(
        child: Center(
            child: Column(
          children: [
            SizedBox(
              width: 200,
              child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(context, createRouteTransition(HomeReto1()));
                  },
                  child: Text('Reto Simple 1')),
            ),
            SizedBox(
              width: 200,
              child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(context, createRouteTransition(HomeReto2()));
                  },
                  child: Text('Reto Simple 2')),
            ),
            SizedBox(
              width: 200,
              child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(context, createRouteTransition(LoadingReto3()));
                  },
                  child: Text('Reto Simple 3')),
            ),
            SizedBox(
              width: 200,
              child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(context, createRouteTransition(HomeReto4()));
                  },
                  child: Text('Reto Intermedio 1')),
            ),
          ],
        )),
      ),
    );
  }
}
