import 'package:flutter/material.dart';

class BottomCardProfile extends StatelessWidget {
  const BottomCardProfile({
    Key? key,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      width: double.infinity,
      height: size.height * 0.46,
      color: Colors.white,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 18, vertical: 24),
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.black26,
                  offset: Offset(0, 6),
                  blurRadius: 10)
            ],
            borderRadius: BorderRadius.circular(18)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _CardImage(size: size),
            const SizedBox(
              height: 8,
            ),
            _CardInfo(),
            Padding(
              padding: const EdgeInsets.only(left: 10, top: 8),
              child: Text('42 E 20th St New York 23 USA', style: TextStyle(color: Colors.blueGrey, fontSize: 16, fontFamily: 'JosefinSans'),),
            )
          ],
        ),
      ),
    );
  }
}

class _CardImage extends StatelessWidget {
  const _CardImage({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topRight,
      height: size.height * 0.29,
      width: double.infinity,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/reto4/food.jpg'),
              fit: BoxFit.cover),
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(18),
            topLeft: Radius.circular(18),
          )),
      child: Container(
        height: 26,
        width: 60,
        margin: const EdgeInsets.only(top: 14, right: 14),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Icon(
              Icons.star,
              color: Colors.yellow,
              size: 19,
            ),
            Text('4.2', style: TextStyle(fontFamily: 'JosefinSans'),)
          ],
        ),
      ),
    );
  }
}

class _CardInfo extends StatelessWidget {
  const _CardInfo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'Gramercy Tavern',
            style: TextStyle(fontSize: 19, fontFamily: 'JosefinSans'),
          ),
          const SizedBox(width: 2,),
          Container(
            height: 24,
            width: 64,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [
                  Color(0xfffaab91),
                  Color(0xfffe93a3)
                ]
              ),
              borderRadius: BorderRadius.circular(30)
            ),
            child: Text('Italian', style: TextStyle(fontFamily: 'JosefinSans'),),
          ),
          const SizedBox(width: 2,),
          Stack(
            children: [
              //Spread operator
              for (var i = 4; i > 0; i--) ...[
                Container(
                  padding: const EdgeInsets.all(1.5),
                  margin: EdgeInsets.only( left:(i-1)*12),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle
                  ),
                  child: CircleAvatar(
                    backgroundImage: AssetImage('assets/reto4/profile$i.png'),
                    radius: 12,
                  ),
                )
              ]
            ],
          ),
          Icon(Icons.more_vert_rounded, size: 20,)
        ],
      ),
    );
  }
}