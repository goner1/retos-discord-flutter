import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  const Profile({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      alignment: Alignment.center,
      height: size.height * 0.6,
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            'My Profile',
            style: TextStyle(fontSize: 26, fontFamily: 'JosefinSans'),
          ),
          CircleAvatar(
            backgroundImage: AssetImage('assets/reto4/profile.png'),
            radius: 50,
          ),
          Text.rich(
            TextSpan(
              text: 'John Williams\n',
              style: TextStyle(fontSize: 26, color: Color(0xff151B63), fontFamily: 'JosefinSans'),
              children: [
                TextSpan(
                  text: 'john.williams@gmail.com',
                  style: TextStyle(color: Colors.blueGrey, fontSize: 16, fontFamily: 'JosefinSans'),
                )
              ]
            ),
            textAlign: TextAlign.center,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              _StatProfile(
                stat: '250',
                nameStat: 'Reviews',
              ),
              _CustomDivider(),
              _StatProfile(
                stat: '100k',
                nameStat: 'Fallowers',
              ),
              _CustomDivider(),
              _StatProfile(
                stat: '30',
                nameStat: 'Following',
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              _ButtonsProfile(
                size: size,
                text: 'Edit Profile',
                isEdit: true,
                callback: ()=> print('edit profile'),
              ),
              _ButtonsProfile(
                size: size,
                text: 'Settings',
                isEdit: false,
                callback: ()=> print('settings'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class _StatProfile extends StatelessWidget {
  final String stat;
  final String nameStat;

  const _StatProfile({required this.stat, required this.nameStat});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          stat,
          style: TextStyle(color: Color(0xff5563FF), fontSize: 22, fontFamily: 'JosefinSans'),
        ),
        Text(
          nameStat,
          style: TextStyle(color: Colors.blueGrey, fontSize: 16, fontFamily: 'JosefinSans'),
        )
      ],
    );
  }
}

class _CustomDivider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      width: 1.5,
      color: Colors.grey[300],
    );
  }
}

class _ButtonsProfile extends StatelessWidget {
  final Size size;
  final String text;
  final bool isEdit;
  final VoidCallback callback;

  const _ButtonsProfile({ 
    required this.size, 
    required this.text, 
    required this.isEdit, 
    required this.callback});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 46,
      width: size.width * 0.4,
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: isEdit ? Color(0xff5563FF):Colors.white,
            shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6),
                  side: isEdit
                      ? BorderSide.none
                      : BorderSide(color: Colors.grey)),
            onPrimary: isEdit ? Colors.white : Colors.grey,
            // fixedSize: Size(300, 80)
          ),
          onPressed: callback,
          child: Text(
            text,
            style: TextStyle(color: isEdit ? Colors.white : Colors.blueGrey, fontSize: 16, fontFamily: 'JosefinSans'),
          )),
    );
  }
}
