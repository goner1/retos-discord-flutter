import 'package:flutter/material.dart';
import 'package:retos_discord_flutter/src/pages/reto4/widgets/bottom_card_profile.dart';
import 'package:retos_discord_flutter/src/pages/reto4/widgets/profile.dart';

class HomeReto4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Color(0xffe7eaed),
        body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: [
              Profile(),
              const SizedBox(
                height: 10,
              ),
              BottomCardProfile()
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavBar(),
      floatingActionButton: Container(
        height: 48,
        width: 48,
        child: FloatingActionButton(
          backgroundColor: Color(0xff5563FF),
          onPressed: (){},
          child: Icon(Icons.add, size: 30,),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}

class BottomNavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: kBottomNavigationBarHeight,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Icon(
            Icons.home,
            color: Color(0xff5563FF),
            size: 30,
          ),
          Icon(
            Icons.search,
            color: Colors.grey,
            size: 30,
          ),
          Icon(
            Icons.notifications,
            color: Colors.grey,
            size: 30,
          ),
          Icon(
            Icons.person,
            color: Colors.grey,
            size: 30,
          ),
        ],
      ),
    );
  }
}
