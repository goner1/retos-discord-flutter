import 'package:flutter/material.dart';


class HomeReto3 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Page'),
        actions: [
          IconButton(
            onPressed: ()=> Navigator.pushReplacementNamed(context, 'loading3'),
            icon: Icon(Icons.logout)
          )
        ],
      ),
      body: Center(
        child: Text('Hola Mundo'),
     ),
   );
  }
}