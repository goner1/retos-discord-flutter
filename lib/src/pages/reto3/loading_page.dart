import 'package:flutter/material.dart';

class LoadingReto3 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircularProgressIndicator(),
          const SizedBox(
            height: 16,
          ),
          Text('Cargando...'),
          const SizedBox(
            height: 16,
          ),
          _PercentIndicator(),
        ],
      ),
    );
  }
}

class _PercentIndicator extends StatefulWidget {
  @override
  __PercentIndicatorState createState() => __PercentIndicatorState();
}

class __PercentIndicatorState extends State<_PercentIndicator> {
  double percent = 0.0;
  final border = BorderRadius.circular(20);

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) => llamadaPesada1());
  }

  Future llamadaPesada1() async {
    setState(() {
      percent = 0.33;
    });
    return await Future.delayed(Duration(seconds: 2), llamadaPesada2);
  }

  Future llamadaPesada2() async {
    setState(() {
      percent = 0.66;
    });
    return await Future.delayed(Duration(seconds: 1), llamadaPesada3);
  }

  Future llamadaPesada3() async {
    setState(() {
      percent = 0.9;
    });
    return await Future.delayed(Duration(seconds: 1),
        () => Navigator.pushReplacementNamed(context, 'home3'));
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width * 0.64;

    return Center(
      child: Container(
        height: 10,
        alignment: Alignment.centerLeft,
        width: width,
        decoration:
            BoxDecoration(color: Colors.blue[200], borderRadius: border),
        child: AnimatedContainer(
          duration: Duration(milliseconds: 300),
          width: width * percent,
          decoration: BoxDecoration(color: Colors.blue, borderRadius: border),
        ),
      ),
    );
  }
}
