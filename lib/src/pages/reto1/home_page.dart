import 'package:flutter/material.dart';

class HomeReto1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Column(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 20, top: 10),
                      child: Text(
                        'SALES TOP',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Image(
                      image: AssetImage('assets/reto1/image.png'),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  children: [
                    Center(
                      child: Text(
                        'Hello!',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 28,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    const SizedBox(height: 12,),
                    Text(
                      'Welcome toSales TOP A Platform To \n Manage Real Estate Needs.',
                      style: TextStyle(color: Colors.grey),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: height * 0.07
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        _Button(
                          title: 'Login',
                          isLogin: true,
                        ),
                        const SizedBox(
                          width: 12,
                        ),
                        _Button(
                          title: 'Sign Up',
                          isLogin: false,
                        )
                      ],
                    ),
                    SizedBox(
                      height: height * 0.07,
                    ),
                    Text(
                      'Or via social media',
                      style: TextStyle(
                          color: Colors.grey, fontWeight: FontWeight.w600),
                    ),
                    const SizedBox(
                      height: 14,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        _SocialMedia(
                          img: 'assets/reto1/f.jpg',
                          callback: () => print('facebook'),
                        ),
                        const SizedBox(
                          width: 14,
                        ),
                        _SocialMedia(
                          img: 'assets/reto1/g.png',
                          callback: () => print('google'),
                        ),
                        const SizedBox(
                          width: 14,
                        ),
                        _SocialMedia(
                          img: 'assets/reto1/in.png',
                          callback: () => print('linkedIn'),
                        ),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }
}

class _SocialMedia extends StatelessWidget {
  final String img;
  final VoidCallback callback;

  const _SocialMedia({required this.img, required this.callback});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: this.callback,
      child: Container(
        height: 38,
        width: 38,
        decoration:
            BoxDecoration(image: DecorationImage(image: AssetImage(this.img))),
      ),
    );
  }
}

class _Button extends StatelessWidget {
  final String title;
  final bool isLogin;

  const _Button({
    required this.title,
    required this.isLogin,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(
              (isLogin) ? Color(0xff044df8) : Colors.white),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                  side: isLogin
                      ? BorderSide(color: Colors.transparent)
                      : BorderSide(color: Colors.black))),
          elevation: MaterialStateProperty.all(0),
        ),
        onPressed: () {},
        child: Container(
            alignment: Alignment.center,
            width: 84,
            child: Text(
              this.title,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: isLogin ? Colors.white : Colors.black),
            )));
  }
}
