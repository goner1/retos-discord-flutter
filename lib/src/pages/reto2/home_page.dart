import 'package:flutter/material.dart';
import 'package:retos_discord_flutter/src/pages/reto2/widgets/first_card.dart';
import 'package:retos_discord_flutter/src/pages/reto2/widgets/second_card.dart';

class HomeReto2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size.width;
    final kPadding = size * 0.056;

    return Scaffold(
        backgroundColor: Color(0xffECF0F3),
        body: SafeArea(
            child: Column(
          children: [
            const SizedBox(
              height: 24,
            ),
            FirstCard(
              kPadding: kPadding,
            ),
            const SizedBox(
              height: 24,
            ),
            SecondCard(kPadding: kPadding)
          ],
        )));
  }
}
