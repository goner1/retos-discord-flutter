import 'package:flutter/material.dart';

class FirstCard extends StatelessWidget {
  const FirstCard({
    required this.kPadding,
  });

  final double kPadding;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomCenter,
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: kPadding),
      height: 170,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              stops: [
                0.1,
                0.3,
                0.7,
                1
              ],
              colors: [
                Color(0xffcff1fe),
                Color(0xffe9cef4),
                Color(0xfffca99e).withOpacity(0.88),
                Color(0xffffdc74),
              ])),
      child: Container(
        width: double.infinity,
        height: 170,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            gradient: RadialGradient(
                center: Alignment.bottomCenter,
                focal: Alignment(0.0, 2),
                focalRadius: 1.5,
                radius: 0.2,
                stops: [
                  0.0,
                  0.5,
                  1
                ],
                colors: [
                  Color(0xffe9cef4).withOpacity(0.0),
                  Color(0xfff1aaab).withOpacity(0.6),
                  Color(0xfffedf73).withOpacity(0.6),
                ])),
        child: Stack(
          children: [
            Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: const EdgeInsets.only(right: 8, top: 5),
                  child: Icon(Icons.close),
                )),
            Positioned(
              bottom: -25,
              right: 0,
              child: Image(
                image: AssetImage('assets/reto2/1.png'),
                height: 200,
                width: 180,
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 18),
                child: Text(
                  'you did it! here\nyou can manage\nyour alarm,\nchange time and\nother things',
                  style: TextStyle(
                      color: Colors.black.withOpacity(0.72),
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      height: 1.2),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}