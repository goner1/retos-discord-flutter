import 'package:flutter/material.dart';

class SecondCard extends StatelessWidget {
  const SecondCard({
    required this.kPadding,
  });

  final double kPadding;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: kPadding),
      height: 320,
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(16)),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: kPadding),
        child: Column(
          children: [
            SizedBox(
              height: kPadding,
            ),
            _AlarmSwich(),
            SizedBox(
              height: kPadding/1.4,
            ),
            Divider(color: Colors.grey,),
            SizedBox(
              height: kPadding,
            ),
            _ToBed(),
            _HourSleep(kPadding: kPadding),
            _WakeUp()
          ],
        ),
      ),
    );
  }
}

class _AlarmSwich extends StatelessWidget {
  const _AlarmSwich() ;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          'alarm',
          style: TextStyle(
            color: Colors.grey,
          ),
        ),
        Spacer(),
        Container(
          width: 56,
          height: 32,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(40),
            color: Color(0xff242B32),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'on',
                style: TextStyle(color: Colors.grey),
              ),
              const SizedBox(
                width: 3,
              ),
              Container(
                height: 24,
                width: 24,
                decoration:
                    BoxDecoration(color: Colors.white, shape: BoxShape.circle),
              )
            ],
          ),
        )
      ],
    );
  }
}

class _ToBed extends StatelessWidget {
  const _ToBed();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(Icons.bedtime, ),
        const SizedBox(width: 10,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('to bed', style: TextStyle(color: Colors.grey, ),),
            Text('23:30', style: TextStyle(fontSize: 30, fontWeight: FontWeight.w600),)
          ],
        ),
      ],
    );
  }
}

class _HourSleep extends StatelessWidget {
  const _HourSleep({
    required this.kPadding,
  }) ;

  final double kPadding;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _CustomDivider(kPadding: kPadding),
        const SizedBox(height: 10,),
        Row(
          children: [
            Icon(Icons.snooze_rounded, color: Colors.grey,),
            const SizedBox(width: 10,),
            Text('8 hours sleep', style: TextStyle(color: Colors.grey,),),
            Spacer(),
            Icon(Icons.arrow_forward_ios_outlined, size: 20),
          ],
        ),
        const SizedBox(height: 10,),
        _CustomDivider(kPadding: kPadding),
      ],
    );
  }
}

class _CustomDivider extends StatelessWidget {
  const _CustomDivider({
    required this.kPadding,
  }) ;

  final double kPadding;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 22,
      width: 1,
      color: Colors.grey[300],
      margin: EdgeInsets.only(left: kPadding/2,),
    );
  }
}



class _WakeUp extends StatelessWidget {
  const _WakeUp();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(Icons.wb_sunny),
        const SizedBox(width: 10,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('07:30', style: TextStyle(fontSize: 30, fontWeight: FontWeight.w600),),
            Text('wake up', style: TextStyle(color: Colors.grey, ),),
          ],
        ),
      ],
    );
  }
}