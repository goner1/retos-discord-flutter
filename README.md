# Retos discord Locos por Flutter y Dart

## 01
<TABLE BORDER>
    <TR>
        <TH style="text-align:center">RETO SIMPLE 1</TH>
	<TR>
		<TD><img src="./screenshot/reto1/1.PNG" alt="Home" width="200"/></TD> 
	</TR>
</TABLE>

## 02
<TABLE BORDER>
    <TR>
        <TH style="text-align:center">RETO SIMPLE 2</TH>
	<TR>
		<TD><img src="./screenshot/reto2/1.PNG" alt="Home" width="200"/></TD> 
	</TR>
</TABLE>

## 03
<TABLE BORDER>
    <TR>
        <TH style="text-align:center">RETO SIMPLE 3</TH>
	<TR>
		<TD><img src="./screenshot/reto3/1.gif" alt="Home" width="200"/></TD> 
	</TR>
</TABLE>

## 04
<TABLE BORDER>
    <TR>
        <TH style="text-align:center">RETO INTERMEDIO 1</TH>
	<TR>
		<TD><img src="./screenshot/reto4/1.png" alt="Home" width="400"/></TD> 
	</TR>
</TABLE>